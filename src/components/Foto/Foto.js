import React, { Component } from 'react';
import FotoHeader from '../FotoHeader/FotoHeader'
import FotoInfo from '../FotoInfo/FotoInfo'
import FotoAtualizacoes from '../FotoAtualizacoes/FotoAtualizacoes'
import './Foto.css'

export default class Foto extends Component {
  render () {
    return (
      <div className="foto">
        <FotoHeader {...this.props.user}/>
        <img alt="foto" className="foto-src" src={this.props.image}/>
        <FotoInfo liked={this.props.liked} comments={this.props.comments}/>
        <FotoAtualizacoes/>
      </div>
    );
  }
}