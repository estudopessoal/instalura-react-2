import React, { Component } from 'react';
import './FotoHeader.css'

export default class FotoHeader extends Component {
  render () {
    return (
      <header className="foto-header">
        <figure className="foto-usuario">
          <img src={this.props.image} alt={this.props.name}/>
          <figcaption className="foto-usuario">{this.props.name}</figcaption>
        </figure>
      </header>
    );
  }
}