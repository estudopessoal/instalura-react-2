import React, { Component } from 'react'
import Foto from '../Foto/Foto'
import './Timeline.css'

export default class Timeline extends Component {
  
  constructor() {
    super()
    this.state = {
      fotos: []
    }
  }

  componentDidMount() {
    fetch('/api/timeline.json')
      .then(res => res.json())
      .then(fotos => this.setState({fotos}))
  }

  render () {
    return (
      <div className="timeline container">
        {
          this.state.fotos.map((foto, index) => 
            <Foto
              key={index}
              user={foto.user}
              image={foto.image}
              liked={foto.liked}
              comments={foto.comments}
            />)
        }
      </div>
    )
  }
}