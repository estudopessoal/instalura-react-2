import React, { Component } from 'react'
import Header from '../../components/Header/Header'
import Timeline from '../../components/Timeline/Timeline'

import 'normalize.css/normalize.css'
import './App.css'

export default class App extends Component {
  render() {
    return (
      <div className="app">
        <Header/>
        <Timeline/>
      </div>
    )
  }
}